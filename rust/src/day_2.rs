pub mod rust {
    // TEST
    pub fn test() {
        ownership();
        mem_alloc();
    }

    // Ownership 
    fn ownership(){
        let x = String::from("Variable of top scope!");
        {
            println!("{x}");
            let x = String::from("Variable of lower scope!");
            println!("{x}");
        }
        println!("{x}");
    }

    // Memory allocation
    fn mem_alloc(){
        let mut n1 = 10;
        let n2 = n1;
        n1 = 100;
        println!("{n1} {n2}");

        let str1 = String::from("hello");
        let str2 = str1.clone();
        println!("{str1} {str2}");
    }
}

